## Live facial landmarks detection using openFrameworks and openCV


### setup

1. Download and install [openFrameworks](https://openframeworks.cc/download/)

2. Download the [ofxCv](https://github.com/kylemcdonald/ofxCv) addon

3. Check supported video capture resolutions (to be set accordingly in `ofApp.h`), e.g.:
```
$ sudo apt install v4l2-utils
$ v4l2-ctl --list-formats-ext`
```

4. Set appropriate screen resolution in `main.cpp`

5. adjust `OF_ROOT` path in `Makefile`

6. compile and run:
```
$ make && make run
```


### usage

Keyboard shortcuts can be used to toggle visual marks

| key | action                                          |
| :-: | :--                                             |
| b   | show / hide facial landmarks bounding boxes     |
| l   | show / hide facial landmarks polylines          |
| i   | show / hide facial landmarks indexes            |
| f   | show / hide facial landmarks labels (fonts)     |

<img src="preview.jpg" width="300"/>


### resources


#### Haar Cascade

- [HaarFinder example](https://github.com/openframeworks/openFrameworks/tree/master/examples/computer_vision/opencvHaarFinderExample)

- [CascadeClassifier reference](https://docs.opencv.org/4.x/d1/de5/classcv_1_1CascadeClassifier.html)

- [ofxCvHaarFinder source](https://github.com/openframeworks/openFrameworks/blob/master/addons/ofxOpenCv/src/ofxCvHaarFinder.h)

- [ofxFaceTracker addon](https://github.com/kylemcdonald/ofxFaceTracker) (breaks for OF 0.11)

- [ofxFaceTracker2 addon](https://github.com/HalfdanJ/ofxFaceTracker2) (must compile [dlib](http://dlib.net/))

- [Haar cascade classifiers](https://github.com/opencv/opencv/tree/master/data/haarcascades)


#### Face landmark

- [Facemark API tutorial](https://docs.opencv.org/4.x/d7/dec/tutorial_facemark_usage.html)

- [Facemark class reference](https://docs.opencv.org/4.x/db/dd8/classcv_1_1face_1_1Facemark.html)

- [LBF model sample](https://raw.githubusercontent.com/kurnianggoro/GSOC2017/master/data/lbfmodel.yaml)

