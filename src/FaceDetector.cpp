#include "FaceDetector.h"

using namespace std;
using namespace cv;
using namespace cv::face;


FaceDetector::FaceDetector(void){};


void FaceDetector::setup(){

  if(!faceCascade.load("data/" + faceCascadeFile)){
    std::printf("ERROR: failed to load cascade classifier!");
  }

  facemark = FacemarkLBF::create();
  facemark->loadModel("data/" + facemarkModelFile);

}


void FaceDetector::setScale(float scaleX, float scaleY){
  drawScaleX = scaleX;
  drawScaleY = scaleY;
}


bool FaceDetector::update(InputArray input){

  if(faceCascade.empty()) return false;

  /* convert to gray scale */
  Mat gray;
  if (input.channels() > 1){
    cvtColor(input, gray, COLOR_BGR2GRAY);
  }
  else{
    gray = input.getMat().clone();
  }
  equalizeHist(gray, gray);

  /* adjust min/max detect size here */
  int max_px = gray.cols < gray.rows ? gray.cols : gray.rows;
  int min_px = max_px/100;
  cv::Size minDetectSize = cv::Size(min_px, min_px);
  cv::Size maxDetectSize = cv::Size(max_px, max_px);

  /* detect faces using haar cascade classifier */
  faceCascade.detectMultiScale(gray,
                               faces,
                               haarScaleFactor,
                               minDetectNeighbors,
                               0,
                               minDetectSize,
                               maxDetectSize);

  /* try landmark detection */
  if(faces.size() > 0) {
    return facemark->fit(gray, faces, landmarks);
  }
  return false;
}


void FaceDetector::drawFaces(int lineWidth, ofColor color){
  ofPushStyle();
  ofSetLineWidth(lineWidth);
  ofSetColor(color);

  for(unsigned int i = 0; i < faces.size(); i++) {
    ofDrawRectangle(faces[i].x*drawScaleX,
                    faces[i].y*drawScaleY,
                    faces[i].width*drawScaleX,
                    faces[i].height*drawScaleY);
  }
  ofPopStyle();

}


void FaceDetector::drawLandmark(vector<int> indexes, const string &label, ofTrueTypeFont* font, ofColor color){

  ofPushStyle();
  ofSetColor(color);

  for(unsigned int i = 0; i < faces.size(); i++) {
    if(landmarks[i].size() != 68) continue;  // does not matchLBF model

    ofPolyline polyline;
    ofPoint topLeft(-1, -1);
    ofPoint bottomRight(0, 0);

    for(unsigned int j : indexes){
      Point2f p = landmarks[i][j];
      float x = p.x*drawScaleX;
      float y = p.y*drawScaleY;

      /* update landmark bounding box */
      if(topLeft.x == -1 || x < topLeft.x) topLeft.x = x;
      if(topLeft.y == -1 || y < topLeft.y) topLeft.y = y;
      if(x > bottomRight.x) bottomRight.x = x;
      if(y > bottomRight.y) bottomRight.y = y;

      if(showLandmarkLine){
        polyline.addVertex({x, y, 0.1});
      }

      if(showLandmarkIndexes){
        ofDrawBitmapString(to_string(j), x, y);
      }

    }
    polyline.close();
    polyline.draw();

    ofRectangle landmarkBox(topLeft, bottomRight);
    if(showLandmarkBox){
      ofDrawRectangle(landmarkBox);
    }

    if(showLandmarkLabels){
      ofFill();
      ofRectangle stringBox = font->getStringBoundingBox(label, landmarkBox.getLeft(), landmarkBox.getBottom());
      float fontScaleX = landmarkBox.width/stringBox.width;
      float fontScaleY = landmarkBox.height/stringBox.height;
      ofPushMatrix();
      ofScale(fontScaleX, fontScaleY, 1);
      font->drawStringAsShapes(label, landmarkBox.getLeft()/fontScaleX, landmarkBox.getBottom()/fontScaleY);
      ofPopMatrix();
      ofNoFill();
    }
  }
  ofPopStyle();
}
