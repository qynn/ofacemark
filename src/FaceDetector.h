#pragma once

#include "ofxOpenCv.h"
#include <opencv2/face/facemarkLBF.hpp>


class FaceDetector{

	public:

    FaceDetector(void);

    /* public toggles */
    bool showLandmarkIndexes = true;
    bool showLandmarkBox = true;
    bool showLandmarkLine = true;
    bool showLandmarkLabels = true;

    /**
     * Must be called before using other methods
     */
    void setup();


    /**
     * Update draw scale ratios
     *
     * @param scaleX    : horizontal scale ratio
     * @param scaleY    : vertical scale ratio
     */
    void setScale(float scaleX, float scaleY);


    /**
     * Detect faces and landmarks
     *
     * @param input : source image matrix
     * @return true if facemark detection succeeded
     */
    bool update(cv::InputArray input);


    /**
     * Draw rectangles around detected faces
     *
     * @param lineWidth : rectangles line width
     * @param color     : rectangles line color
     */
    void drawFaces(int lineWidth = 3, ofColor color = ofColor::black);


    /**
     * Draw landmark polyline and label
     *
     * @param indexes : LBF landmark indexes (see landmarks.h)
     * @param label : text to be displayed on landmark
     * @param font : font object to be used for label
     * @param color : label color
     */
    void drawLandmark(vector<int> indexes, const string &label, ofTrueTypeFont* font, ofColor color);

 private:

    /* Haar Cascade */
    cv::CascadeClassifier faceCascade;
    vector<cv::Rect> faces;  //holds cascade classifier output
    cv::String faceCascadeFile = "haarcascade_frontalface_default.xml";
    float haarScaleFactor = 1.08;
    int minDetectNeighbors = 5;

    /* Facial landmarks */
    cv::Ptr<cv::face::Facemark> facemark;
    vector<vector<cv::Point2f> > landmarks;  // facemark output
    cv::String facemarkModelFile = "facemark_lbfmodel.yaml";

    /* Scaling */
    float drawScaleX = 1.0;
    float drawScaleY = 1.0;

};
