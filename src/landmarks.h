#pragma once

/* Face landmark indexes (LBF model)*/

static const vector<int> LEFT_EYE = {36, 37, 38, 39, 40, 41};

static const vector<int> RIGHT_EYE = {42, 43, 44, 45, 46, 47};

static const vector<int> LEFT_BROW = {17, 18, 19, 20, 21};

static const vector<int> RIGHT_BROW = {22, 23, 24, 25, 26};

static const vector<int> NOSE = {27, 28, 29, 30, 31, 32, 33, 34, 35, 30};

static const vector<int> MOUTH = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};

static const vector<int> JAW = {0, 1, 2, 3, 4, 5, 6, 7, 8,
                                9, 10, 11, 12, 13, 14, 15, 16};
