#include "ofApp.h"


void ofApp::setup(){

  videoGrabber.setVerbose(true);
  videoGrabber.setup(cameraWidth, cameraHeight);

  faceDetector.setup();

  serifFont.load(OF_TTF_SERIF, fontSize, true, true, true, 0.0f);
  sansFont.load(OF_TTF_SANS, fontSize, true, true, true, 0.0f);
  monoFont.load(OF_TTF_MONO, fontSize, true, true, true, 0.0f);
}


void ofApp::update(){

  videoGrabber.update();

	if (videoGrabber.isFrameNew()){

    cv::Mat grabberMat = ofxCv::toCv(videoGrabber);
    faceDetector.update(grabberMat);

    feed.setFromPixels(videoGrabber.getPixels());
  }
}


void ofApp::draw(){

  int width = ofGetWidth();
  int height = ofGetHeight();

  ofNoFill();

  /* show video feed */
  feed.draw(0, 0, width, height);

  /* draw faces */
  faceDetector.setScale(width/cameraWidth, height/cameraHeight);
  faceDetector.drawFaces(3, ofColor::black);

  /* draw landmarks */
  faceDetector.drawLandmark(LEFT_EYE, "left eye", &sansFont, ofColor::red);
  faceDetector.drawLandmark(RIGHT_EYE, "right eye", &serifFont, ofColor::red);

  faceDetector.drawLandmark(LEFT_BROW, "left brow", &sansFont, ofColor::blue);
  faceDetector.drawLandmark(RIGHT_BROW, "right brow", &serifFont, ofColor::blue);

  faceDetector.drawLandmark(NOSE, "NOSE", &monoFont, ofColor::purple);
  faceDetector.drawLandmark(MOUTH, "mouth", &monoFont, ofColor::green);

  faceDetector.drawLandmark(JAW, "", &sansFont, ofColor::yellow);
}


void ofApp::windowResized(int w, int h){
  std::printf("window resized to %ix%i (max %ix%i)\n",
              w, h, ofGetScreenWidth(), ofGetScreenHeight());
}


void ofApp::keyPressed(int key){
  switch (key){
    case 'b':
      faceDetector.showLandmarkBox = !faceDetector.showLandmarkBox;
      break;
    case 'l':
      faceDetector.showLandmarkLine = !faceDetector.showLandmarkLine;
      break;
    case 'i':
      faceDetector.showLandmarkIndexes = !faceDetector.showLandmarkIndexes;
      break;
    case 'f':
      faceDetector.showLandmarkLabels = !faceDetector.showLandmarkLabels;
      break;
	}
}
