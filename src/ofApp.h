#pragma once

#include "ofMain.h"
#include "ofxCv.h"
#include "FaceDetector.h"
#include "landmarks.h"

#include "ofxOpenCv.h"
/* #include "ofxCvHaarFinder.h" */


class ofApp : public ofBaseApp{
	public:

    /* App */
		void setup();
		void update();
		void draw();
		void windowResized(int w, int h);
    void keyPressed(int key);

    /* videoGrabber */
    ofVideoGrabber videoGrabber;
    ofxCvColorImage feed;
    static const int cameraWidth = 320;  // 640
    static const int cameraHeight = 180;  // 360

    /* FaceDetector */
    FaceDetector faceDetector;

    /* Style */
    ofTrueTypeFont serifFont, sansFont, monoFont;
    int fontSize = 24;

};
